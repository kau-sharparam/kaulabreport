kaulabreport
============

To use this improved lab report template, put the following in your preamble:

```latex
\usepackage[
    swedish|english,
    coursecode=course code here,
    coursename={course name here},
    semester={semester here},
    exercise={name of exercise/lab},
    teacher={lab teacher},
    group=group (if any),
    name1={Name of first pereson},
    name2={Name of second person (optional argument)},
    name3={Name of third person (optional argument)},
    name4={Name of fourth person (optional argument)}
]{kaulabreport}
```

You should complement this with a call to `\kaussn{ssn1}[ssn2][ssn3][ssn4]` in a separate file not tracked by your versioning system to supply the SSN of the people involved in the lab.

